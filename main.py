import argparse
import asyncio
from urllib.parse import urlencode
import aiohttp
from argument_types.coordinates_types import latitude_type, longitude_type

BASE_METEO_URL = 'https://api.open-meteo.com/v1/forecast?'
BASE_LOCATION_URL = "https://nominatim.openstreetmap.org/reverse?"


async def get_location_name(latitude: float, longitude: float) -> str:
    """
    Gets the name of the location, based on given coordinates. Uses nominatim API and aiohttp for asynchronous request.

    Args:
        latitude: of searched location
        longitude: of searched location

    Returns:
        location: city / town / village name, if available. If not return string with plain coordinates.
    """
    url_parameters_dict = {'lat': f'{latitude}', 'lon': f'{longitude}', 'format': 'json'}
    request_url = BASE_LOCATION_URL + urlencode(url_parameters_dict)

    async with aiohttp.ClientSession() as session:
        async with session.get(request_url) as response:
            data = await response.json()
            address = data.get('address', {})
            # Name of the searched place can be placed under one of the following fields 
            location = address.get('city', '')
            if not location:
                location = address.get('town', '')
            if not location:
                location = address.get('village', '')
            if not location:
                location = f"{latitude}, {longitude}"  # No location name available, use plain coordinates
            return location


async def WeatherFetcher(queue: asyncio.Queue, latitude: float, longitude: float) -> None:
    """
    Queries open-meteo API for weather data for chosen location. Uses aiohttp for asynchronous request.
    Weather results in the form of dictionary and name of the location are put in the queue.

    Args:
        queue: asyncio queue to put api data
        latitude: of chosen location
        longitude: of chosen location
    """
    location_name = await get_location_name(latitude=latitude, longitude=longitude)

    url_parameters_dict = {'latitude': f'{latitude}', 'longitude': f'{longitude}', 'hourly': 'temperature_2m,rain'}
    request_url = BASE_METEO_URL + urlencode(url_parameters_dict)
    # Request meteo data and put them in the queue
    async with aiohttp.ClientSession() as session:
        async with session.get(request_url) as response:
            json_meteo_data = await response.json()
            await queue.put((json_meteo_data, location_name))


async def WeatherProcessor(queue: asyncio.Queue, temp_under_threshold: float, rain_over_threshold: float) -> None:
    """
    Process data put in the queue and print warning if thresholds for temperature and rain are exceeded.

    Args:
        queue: asyncio queue - source of meteo data
        temp_under_threshold: lower threshold of temperature
        rain_over_threshold: upper threshold of rain
    """
    json_meteo_data, location_name = await queue.get()

    # In case of issues, json data indicating errors is returned by meteo API
    if "hourly" not in json_meteo_data:
        print(f"Something is wrong. Returned data doesn't have correct structure: {json_meteo_data}.\nQuitting...")
        queue.task_done()
        return

    # Lists under hourly contain date with hour and corresponding forecast
    for time, temperature, rain in zip(
            json_meteo_data['hourly']['time'], 
            json_meteo_data['hourly']['temperature_2m'],
            json_meteo_data['hourly']['rain']
        ):
        if temperature < temp_under_threshold and rain > rain_over_threshold:
            print(f"{location_name}: Warning, low temperature {temperature} of C and rain {rain} mm expected on {time}")

    queue.task_done()


async def run_fetcher_and_processor(temp_under_threshold: float, rain_over_threshold: float, 
                                    latitude: float, longitude: float) -> None:
    """
    Start fetcher and processor coroutines

    Args:
        temp_under_threshold: lower threshold of temperature
        rain_over_threshold: upper threshold of rain
        latitude: of chosen location
        longitude: of chosen location
    """
    q = asyncio.Queue()

    fetcher_task = asyncio.create_task(WeatherFetcher(queue=q, latitude=latitude, longitude=longitude))
    asyncio.create_task(WeatherProcessor(queue=q, temp_under_threshold=temp_under_threshold, 
                                                  rain_over_threshold=rain_over_threshold))
    
    await fetcher_task
    await q.join()


def main():
    parser = argparse.ArgumentParser(description="Check weather and give warnings if temperature and rain "
                                                 "thresholds are exceeded.")

    parser.add_argument("-t", "--temperature", type=float, help="Temperature lower threshold [C]; Below this value "
                                                                "warning is triggered", default=7)
    parser.add_argument("-r", "--rain", type=float, help="Rain upper threshold [mm];  Below this values warning is "
                                                         "triggered", default=0.2)
    # Limit acceptable values of geographical coordinates arguments
    parser.add_argument("-lat", "--latitude", type=latitude_type, help="Latitude of location to check weather for." 
                        " Acceptable range: (-90.0, 90.0).", default=54.45604639612041)
    parser.add_argument("-long", "--longitude", type=longitude_type, help="Longitude of location to check weather for."
                        " Acceptable range: [180.0, 180.0].", default=18.458081957606122)

    args = parser.parse_args()
    temp_under_threshold = args.temperature
    rain_over_threshold = args.rain
    latitude = args.latitude
    longitude = args.longitude

    asyncio.run(run_fetcher_and_processor(temp_under_threshold, rain_over_threshold, latitude, longitude))


if __name__ == "__main__":
    main()
