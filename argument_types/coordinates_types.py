import argparse


def latitude_type(latitude_value):
    """
    Argparse wrapper for latitude argument, to limit it to [-90.0, 90.0] range.
    """
    try:
        float_lat_value = float(latitude_value)
    except ValueError:
        raise argparse.ArgumentTypeError("Invalid float value: {}".format(latitude_value))
    if not -90.0 <= float_lat_value <= 90.0:
        raise argparse.ArgumentTypeError("Value of geographical coordiantes must be between -90.0 and 90.0")
    return float_lat_value


def longitude_type(longitude_value):
    """
    Argparse wrapper for longitude argument, to limit it to [-180.0, 180.0] range.
    """
    try:
        float_long_value = float(longitude_value)
    except ValueError:
        raise argparse.ArgumentTypeError("Invalid float value: {}".format(longitude_value))
    if not -180.0 <= float_long_value <= 180.0:
        raise argparse.ArgumentTypeError("Value of longitude must be between -180.0 and 180.0")
    return float_long_value