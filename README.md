﻿# Meteo Weather
Program checks weather forecast for chosen location and prints warnings if thresholds for rain and temperature are exceeded.

## Installation
On Linux system, to create virtual environment and download necessary packages simply run: 
```bash
$ make
```
Alternatively, create venv manually, activate it and install packages:
```bash
$ pip install -r requirements.txt
```
## Usage
To run program activate virtual environment e.g.:
```bash
$ source venv/bin/activate
```
and run the command:
```bash
$ python main.py -t 8 -r 0.2 -lat 54.35 -long 18.63
```
`-t`, `-r`, `-lat`, `-long` denote recpectively temperature threshold, rain threshold, latitude and longitude of chosen location. All arguments are optional. Program will print warnings if, at given time, temperature is expected to be lower than respective threshold and rainfall is expected to higher.
To run unitests run `pytest` from main project directory.
