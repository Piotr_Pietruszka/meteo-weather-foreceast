import pytest
import asyncio
import main
import json
import os


@pytest.mark.parametrize("latitude, longitude", [
    (51.5, 0),
    (54.1234, 19.3811),
    (35.08, -106.64),
])
@pytest.mark.asyncio
async def test_WeatherFetcher(latitude: float, longitude: float):
    """
    Tests querying meteo API and verifies structure of returned output

    Args:
        latitude: of chosen location
        longitude: of chosen location
    """
    q = asyncio.Queue()
    await main.WeatherFetcher(queue=q, latitude=latitude, longitude=longitude)
    queue_entry_data, _ = await q.get()

    # Check that expected entries are included in the output dictionary
    assert "hourly" in queue_entry_data, "\"hourly\" field not present in the output dictionary"
    assert "time" in queue_entry_data["hourly"], "\"time\" field not present in the output dictionary"
    assert "temperature_2m" in queue_entry_data["hourly"], "\"temperature_2m\" field not present in the output dictionary"
    assert "rain" in queue_entry_data["hourly"], "\"rain\" field not present in the output dictionary"

    assert len(queue_entry_data["hourly"]["time"]) == \
           len(queue_entry_data["hourly"]["temperature_2m"]) == \
           len(queue_entry_data["hourly"]["rain"]), "time and weather lists have different lengths"


@pytest.mark.parametrize("latitude, longitude, expected_location_name", [
    (54.01, 17.94, "Wdzydze"),
    (50.27, 19.13, "Sosnowiec"),
    (-22.91, -43.18, "Rio de Janeiro"),
    (21.37, 21.37, "21.37, 21.37"),
])
@pytest.mark.asyncio
async def test_get_location_name(latitude: float, longitude: float, expected_location_name: str):
    """
    Tests querying meteo API and verifies structure of returned output

    Args:
        latitude: of chosen location
        longitude: of chosen location
        expected_location_name (string): expected output
    """
    location_name = await main.get_location_name(latitude=latitude, longitude=longitude)
    assert location_name == expected_location_name


@pytest.mark.parametrize("input_filename, output_filename", [
    ("WeatherProcessor_test_input_1.json", "WeatherProcessor_test_output_1.txt"),
    ("WeatherProcessor_test_input_2.json", "WeatherProcessor_test_output_2.txt"),
])
@pytest.mark.asyncio
async def test_WeatherProcessor(input_filename: str, output_filename: str, capsys):
    """
    Test parsing (printing warnings) on example data. Due to the size input and output data are stored in the 
    seperate files.

    Args:
        input_filename: json file with threshold, location name and dictionaty with API data
        output_filename: txt file with printed warnings
        capsys: fixture to capture stdout
    """
    # Read input and output data from files
    input_filename = os.path.join("tests", input_filename)
    output_filename = os.path.join("tests", output_filename)
    with open(input_filename, "r") as json_in_file:
        example_in_data = json.load(json_in_file)
    with open(output_filename, "r") as txt_out_file:
        expected_warnings = txt_out_file.read()

    q = asyncio.Queue()
    await q.put((example_in_data["meteo_data"], example_in_data["location_name"]))
    await main.WeatherProcessor(queue=q, temp_under_threshold=example_in_data["temperature_under_threshold"], rain_over_threshold=example_in_data["rain_over_threshold"])
    output = capsys.readouterr()

    assert output.out == expected_warnings, f"Expected: \n{expected_warnings}\n-------------------\nGot: \n{output.out}"



@pytest.mark.parametrize("input_filename, output_filename", [
    ("WeatherProcessor_test_input_1.json", "WeatherProcessor_test_output_1.txt"),
    ("WeatherProcessor_test_input_2.json", "WeatherProcessor_test_output_2.txt"),
])
@pytest.mark.asyncio
async def test_WeatherProcessor(input_filename: str, output_filename: str, capsys):
    """
    Test parsing (printing warnings) on example data. Due to the size input and output data are stored in the 
    seperate files.

    Args:
        input_filename: json file with threshold, location name and dictionaty with API data
        output_filename: txt file with printed warnings
        capsys: fixture to capture stdout
    """
    # Read input and output data from files
    input_filename = os.path.join("tests", input_filename)
    output_filename = os.path.join("tests", output_filename)
    with open(input_filename, "r") as json_input_file:
        example_input_data = json.load(json_input_file)
    with open(output_filename, "r") as txt_output_file:
        expected_warnings = txt_output_file.read()

    q = asyncio.Queue()
    await q.put((example_input_data["meteo_data"], example_input_data["location_name"]))
    await main.WeatherProcessor(queue=q,
                                temp_under_threshold=example_input_data["temperature_under_threshold"],
                                rain_over_threshold=example_input_data["rain_over_threshold"])
    output = capsys.readouterr()

    assert output.out == expected_warnings, f"Warnings mismatch. Expected: \n{expected_warnings}\n"\
                                            f"-------------------\nGot: \n{output.out}"
